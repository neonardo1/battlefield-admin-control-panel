<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/14/19, 11:55 AM
 */

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Adkats\Ban;
use App\Models\Adkats\Record;
use App\Models\Battlefield\Server\Server;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class LandingController
 *
 * @package App\Http\Controllers\Guest
 */
class LandingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $latestBans = Cache::remember('guest.home.latestbans', Carbon::now()->addMinutes(10), function () {
            return Ban::with('player', 'record.server')
                ->active()
                ->orderBy('ban_startTime', 'desc')
                ->take(30)
                ->get();
        });

        $servers = Cache::remember('guest.home.serverlist', Carbon::now()->addMinutes(10), function () {
            return Server::active()->get();
        });

        $recentReports = Cache::remember('guest.home.recentreports', Carbon::now()->addMinutes(10), function () {
            return Record::whereIn('command_type', [18, 20])
                ->with('server')
                ->orderBy('record_time', 'desc')->take(30)->get();
        });

        return view('pages.guest.homepage', compact('latestBans', 'servers', 'recentReports'));
    }
}