<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:05 AM
 */

namespace App\Observers\Battlefield\Server;

use App\Models\Battlefield\Server\Server;
use Illuminate\Support\Facades\Cache;

class ServerObserver
{
    /**
     * Handle the server "created" event.
     *
     * @param \App\Models\Battlefield\Server\Server $server
     *
     * @return void
     */
    public function created(Server $server)
    {
        Cache::forget('guest.home.serverlist');
    }

    /**
     * Handle the server "updated" event.
     *
     * @param \App\Models\Battlefield\Server\Server $server
     *
     * @return void
     */
    public function updated(Server $server)
    {
        Cache::forget('guest.home.serverlist');
    }

    /**
     * Handle the server "deleted" event.
     *
     * @param \App\Models\Battlefield\Server\Server $server
     *
     * @return void
     */
    public function deleted(Server $server)
    {
        //
    }

    /**
     * Handle the server "restored" event.
     *
     * @param \App\Models\Battlefield\Server\Server $server
     *
     * @return void
     */
    public function restored(Server $server)
    {
        //
    }

    /**
     * Handle the server "force deleted" event.
     *
     * @param \App\Models\Battlefield\Server\Server $server
     *
     * @return void
     */
    public function forceDeleted(Server $server)
    {
        //
    }
}
