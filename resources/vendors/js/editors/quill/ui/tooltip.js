/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:57 PM
 */

class Tooltip {
    constructor(quill, boundsContainer) {
        this.quill = quill;
        this.boundsContainer = boundsContainer || document.body;
        this.root = quill.addContainer('ql-tooltip');
        this.root.innerHTML = this.constructor.TEMPLATE;
        let offset = parseInt(window.getComputedStyle(this.root).marginTop);
        this.quill.root.addEventListener('scroll', () => {
            this.root.style.marginTop = (-1 * this.quill.root.scrollTop) + offset + 'px';
            this.checkBounds();
        });
        this.hide();
    }

    checkBounds() {
        this.root.classList.toggle('ql-out-top', this.root.offsetTop <= 0);
        this.root.classList.remove('ql-out-bottom');
        this.root.classList.toggle('ql-out-bottom', this.root.offsetTop + this.root.offsetHeight >= this.quill.root.offsetHeight);
    }

    hide() {
        this.root.classList.add('ql-hidden');
    }

    position(reference) {
        let left = reference.left + reference.width / 2 - this.root.offsetWidth / 2;
        let top = reference.bottom + this.quill.root.scrollTop;
        this.root.style.left = left + 'px';
        this.root.style.top = top + 'px';
        let containerBounds = this.boundsContainer.getBoundingClientRect();
        let rootBounds = this.root.getBoundingClientRect();
        let shift = 0;
        if (rootBounds.right > containerBounds.right) {
            shift = containerBounds.right - rootBounds.right;
            this.root.style.left = (left + shift) + 'px';
        }
        if (rootBounds.left < containerBounds.left) {
            shift = containerBounds.left - rootBounds.left;
            this.root.style.left = (left + shift) + 'px';
        }
        this.checkBounds();
        return shift;
    }

    show() {
        this.root.classList.remove('ql-editing');
        this.root.classList.remove('ql-hidden');
    }
}


export default Tooltip;
