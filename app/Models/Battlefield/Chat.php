<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/14/19, 12:36 AM
 */

namespace App\Models\Battlefield;

use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Chat.
 * @property mixed logSubset
 * @property mixed logDate
 * @property mixed logSoldierName
 * @property mixed logPlayerID
 */
class Chat extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_chatlog';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ID'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['logDate'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['stamp', 'class_css', 'profile_url'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'logPlayerID');
    }

    /**
     * Gets the URL to the players profile.
     *
     * @return string
     */
    public function getProfileUrlAttribute()
    {
        return ! is_null($this->logPlayerID) ? route('player.show', [
            'id'   => $this->logPlayerID,
            'name' => $this->logSoldierName,
        ]) : null;
    }

    /**
     * Excludes server and player chat spam from the result.
     *
     * @param $query
     *
     * @return
     */
    public function scopeExcludeSpam($query)
    {
        return $query->whereNotNull('logPlayerID')->where('logSoldierName', '!=', 'Server')->whereNotIn('logMessage', [
            'ID_CHAT_REQUEST_MEDIC',
            'ID_CHAT_REQUEST_AMMO',
            'ID_CHAT_THANKS',
            'ID_CHAT_REQUEST_RIDE',
            'ID_CHAT_AFFIRMATIVE',
            'ID_CHAT_GOGOGO',
            'ID_CHAT_SORRY',
            'ID_CHAT_ATTACK/DEFEND',
            'ID_CHAT_REQUEST_ORDER',
            'ID_CHAT_GET_IN',
            'ID_CHAT_NEGATIVE',
            'ID_CHAT_GET_OUT',
            'ID_CHAT_REQUEST_REPAIRS',
        ]);
    }

    /**
     * Converts the chat timestamp to an ISO 8601 stamp.
     *
     * @return string
     */
    public function getStampAttribute()
    {
        return $this->logDate->toIso8601String();
    }

    /**
     * Returns the class that should be applied based on the chat visibility.
     *
     * @return string
     */
    public function getClassCssAttribute()
    {
        switch ($this->logSubset) {
            case 'Global':
                $class = 'label bg-teal';
                break;

            case 'Team':
                $class = 'label bg-blue';
                break;

            case 'Squad':
                $class = 'label bg-green';
                break;

            default:
                $class = 'label bg-yellow';
        }

        return $class;
    }
}
