<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Adkats\Account;

use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User.
 * @property mixed user_expiration
 */
class User extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_users';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Fields allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['user_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['user_expiration'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['stamp'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'user_role');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function soldiers()
    {
        return $this->hasMany(Soldier::class, 'user_id');
    }

    /**
     * @return mixed
     */
    public function getStampAttribute()
    {
        return $this->user_expiration->toIso8601String();
    }
}
