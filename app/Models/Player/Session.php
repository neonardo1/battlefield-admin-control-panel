<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Player;

use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Session.
 * @property mixed EndTime
 * @property mixed StartTime
 */
class Session extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_sessions';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'StatsID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['StartTime', 'EndTime'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['session_start', 'session_end'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function server()
    {
        return $this->belongsToMany(\App\Models\Battlefield\Server\Server::class, 'tbl_server_player', 'StatsID',
            'ServerID');
    }

    /**
     * @return mixed
     */
    public function getSessionStartAttribute()
    {
        return $this->StartTime->toIso8601String();
    }

    /**
     * @return mixed
     */
    public function getSessionEndAttribute()
    {
        return $this->EndTime->toIso8601String();
    }
}
