<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 1:43 PM
 */

namespace App\Composers;

use App\Libraries\MenuBuilder;
use Illuminate\View\View;

/**
 * Class MenuComposer
 *
 * @package App\Composers
 */
class MenuComposer
{
    /**
     * @var MenuBuilder
     */
    private $menu;

    /**
     * MenuComposer constructor.
     *
     * @param  MenuBuilder  $menu
     */
    public function __construct(MenuBuilder $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @param  View  $view
     */
    public function compose(View $view): void
    {
        $this->menu->add($this->menu->item('dashboard', 'Dashboard', 'feather icon-home'));
        $this->menu->add($this->menu->item('player', 'Player Listing', 'feather icon-users'));
        $this->menu->add($this->menu->item('servers', 'Servers', 'feather icon-server'));

        // Share all menuData to all the views
        $view->with('menuData', [$this->menu->get()]);
    }
}