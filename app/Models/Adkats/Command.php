<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Adkats;

use App\Models\Elegant;

/**
 * Class Command.
 */
class Command extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_commands';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'command_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['is_interactive', 'is_enabled', 'is_invisible'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Returns the commands that usable by the guest.
     *
     * @param $query
     *
     * @return
     */
    public function scopeGuest($query)
    {
        return $query->where('command_playerInteraction', false);
    }

    /**
     * Returns the commands that usable by the admin.
     *
     * @param $query
     *
     * @return
     */
    public function scopeAdmin($query)
    {
        return $query->where('command_playerInteraction', true);
    }

    /**
     * Returns commands with the selected $type.
     *
     * @param $query
     * @param $type
     *
     * @return mixed
     */
    public function scopeType($query, $type)
    {
        return $query->where('command_active', $type);
    }

    /**
     * Command can be interacted with by player.
     *
     * @return bool
     */
    public function getIsInteractiveAttribute()
    {
        return $this->attributes['command_playerInteraction'] == 1;
    }

    /**
     * Command is enabled.
     *
     * @return bool
     */
    public function getIsEnabledAttribute()
    {
        return $this->attributes['command_active'] == 'Active';
    }

    /**
     * Command is invisible.
     *
     * @return bool
     */
    public function getIsInvisibleAttribute()
    {
        return $this->attributes['command_active'] == 'Invisible';
    }
}
