<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/23/19, 4:05 AM
 */

namespace App\Repository\Battlefield\Scoreboard;


use App\Exceptions\RCONException;
use App\Helpers\Battlefield;
use App\Libraries\BFRCON;
use App\Models\Battlefield\Server\Server;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class LiveScoreboard
 *
 * @package App\Repository\Battlefield\Scoreboard
 */
class LiveScoreboard extends Base
{
    /**
     * @var Server
     */
    protected $server;

    /**
     * @var BFRCON
     */
    protected $rcon;

    /**
     * @var string
     */
    private $team0 = 'Neutral';

    /**
     * @var string
     */
    private $team1 = 'Alpha';

    /**
     * @var string
     */
    private $team2 = 'Bravo';

    /**
     * @var string
     */
    private $team3 = 'Charlie';

    /**
     * @var string
     */
    private $team4 = 'Delta';

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $internal;

    /**
     * LiveScoreboard constructor.
     *
     * @param Server $server
     *
     * @throws Throwable
     */
    public function __construct(Server $server)
    {
        // Load the selected server
        $this->server = $server;
        $this->attempt();
    }

    /**
     * @return $this
     * @throws Throwable
     */
    private function attempt()
    {
        // If we already set the RCON class just return the instance as we don't need to re-connect.
        if (!is_null($this->rcon)) {
            return $this;
        }

        throw_unless(in_array($this->server->game->Name, ['BF3', 'BF4', 'BFH']),
            new RCONException(sprintf("Unrecognized game: %s", $this->server->game->Name)));

        $this->rcon = new BFRCON($this->server, false);

        throw_unless($this->rcon->isConnected(), new RCONException("Not connected to game server"));
        throw_unless(!is_null($this->server->setting),
            new RCONException("Server settings not loaded. Is the server setup in the BFACP?"));

        $this->doLogin();

        return $this;
    }

    /**
     * @return LiveScoreboard
     * @throws Throwable
     */
    private function doLogin()
    {
        $rconPassword = $this->server->setting->getPassword();

        $this->rcon->loginSecure($rconPassword);

        throw_unless($this->rcon->isLoggedIn(), new RCONException("Unable to login to RCON"));

        return $this;
    }

    /**
     * Processes the player list
     *
     * @return LiveScoreboard
     * @throws RCONException
     * @throws Throwable
     */
    public function doGetPlayers()
    {
        $this->doGetServerInfo(true)->setFactions()->doCheckCacheResults();

        $this->internal['players'] = $this->rcon->tabulate($this->rcon->adminGetPlayerlist())['players'];

        $players = new Collection($this->internal['players']);

        $lockedSquads = [
            0 => [],
            1 => [],
            2 => [],
            3 => [],
            4 => [],
        ];

        $playersMapped = [];

        $players->map(function ($player, $key) use (&$lockedSquads) {
            /** @var Battlefield $bfHelper */
            $bfHelper = app(Battlefield::class);
            $player['squadName'] = $bfHelper->squad($player['squadId']);

            if (array_key_exists('ping', $player) && $player['ping'] == 65535) {
                $player['ping'] = null;
            }

            if (array_key_exists($player['squadName'], $lockedSquads[$player['teamId']]) !== true) {
                $lockedSquads[$player['teamId']][$player['squadName']] = $this->rcon->adminGetSquadPrivate($player['teamId'], $player['squadId']);
            }

            $player['kd'] = (app(Battlefield::class))->kd($player['kills'],
                $player['deaths']);

            return $player;
        })->each(function ($player, $key) use (&$playersMapped, &$lockedSquads) {


            switch ($player['teamId']) {
                case 1:
                    $score = (int)$this->internal['server'][9];
                    break;

                case 2:
                    $score = (int)$this->internal['server'][10];
                    break;

                case 3:
                    $score = (int)$this->internal['server'][11];
                    break;

                case 4:
                    $score = (int)$this->internal['server'][12];
                    break;

                default:
                    $score = 0;
                    break;
            }

            if (!isset($playersMapped[$player['teamId']]['team'])) {
                $teamName = $this->getTeamName($player['teamId']);
                $playersMapped[$player['teamId']]['team'] = $teamName;
            }


            // Count the elements in array
            $silen = count($this->internal['server']);

            if (($silen >= 26 && $silen <= 28 && in_array($this->server->game->Name,
                        ['BF4', 'BFHL'])) || ($silen == 25 && $this->server->game->Name == 'BF3')
            ) {
                $playersMapped[$player['teamId']]['score'] = $score;
            } else {
                $playersMapped[$player['teamId']]['score'] = 0;
            }

            $ptype = array_key_exists('type', $player) ? $player['type'] : 0;

            $additional = [
                'isSquadLocked' => array_key_exists($player['squadName'],
                    $lockedSquads[$player['teamId']]) !== false ? $lockedSquads[$player['teamId']][$player['squadName']] : null,
                'squadName' => $player['squadName'],
            ];

            switch ($ptype) {
                case 1:
                    $playersMapped[$player['teamId']]['spectators'][] = $player;
                    break;

                case 2:
                case 3:
                    $playersMapped[$player['teamId']]['commander'] = $player;
                    break;
                default:
                    $playersMapped[$player['teamId']]['players'][] = array_merge($player, $additional);
            }
        });

        $this->data['lockedSquads'] = $lockedSquads;
        $this->data['teams'] = new Collection($playersMapped);
        $this->doGetOnlineAdmins();

        return $this;
    }

    /**
     * Checks the cache and sets if it's cached or not. Used to show on GUI
     *
     * @return $this
     */
    private function doCheckCacheResults()
    {
        $this->data['cached'] = [
            'adminlist' => Cache::has('rcon.db.adminlist'),
        ];

        return $this;
    }

    /**
     * Sets the correct team factions based on game and mode.
     *
     * @return $this
     * @throws Throwable
     */
    protected function setFactions()
    {
        try {
            $factions = in_array($this->server->game->Name,
                ['BF4', 'BFH']) ? $this->rcon->adminVarGetTeamFaction() : trans('rcon.scoreboard.factions');

            if ($this->data['server']['mode']['uri'] == 'RushLarge0') {
                $this->team1 = $factions[0][4];
                $this->team2 = $factions[0][5];
            } else {
                switch ($this->server->game->Name) {
                    case "BF3":
                        $this->team0 = $factions[0][0];
                        $this->team1 = $factions[0][1];
                        $this->team2 = $factions[0][2];
                        $this->team3 = $factions[0][1];
                        $this->team4 = $factions[0][2];
                        break;

                    case "BFHL":
                        $this->team1 = $factions[0][6];
                        $this->team2 = $factions[0][7];
                        $this->team3 = $factions[0][6];
                        $this->team4 = $factions[0][7];
                        break;

                    default:
                        $this->team0 = $factions[0][0];
                        $this->team1 = $factions[0][$factions[1][1] + 1];
                        $this->team2 = $factions[0][$factions[1][2] + 1];
                        $this->team3 = $factions[0][$factions[1][3] + 1];
                        $this->team4 = $factions[0][$factions[1][4] + 1];
                        break;
                }
            }
        } catch (RCONException $e) {
            Log::error($e->getMessage(), $e->getTrace());
        }

        return $this;
    }

    /**
     * Processes the server info
     *
     * @param bool $internalOnly
     *
     * @return LiveScoreboard
     * @throws RCONException
     * @throws Throwable
     */
    public function doGetServerInfo(bool $internalOnly = false)
    {
        // Fetch the server information from the game server.
        $rconServerData = $this->rcon->getServerInfo();

        // Store raw response into a internal variable
        $this->internal['server'] = $rconServerData;

        // If internal flag set don't run any more code.
        if ($internalOnly) {
            return $this;
        }

        // Count how many entries are in the response
        $len = count($rconServerData);

        throw_unless(method_exists($this, 'serverInfoTickets' . $this->server->game->Name),
            new RCONException("Unsupported game"));

        $tickets = $this->{"serverInfoTickets" . $this->server->game->Name}($len, $rconServerData[4], $rconServerData);

        $current_gamemode = $this->rcon->getPlaymodeName($rconServerData[4]);
        $current_map = $this->rcon->getMapName($rconServerData[5]);

        $this->data['server'] = [
            'name' => $rconServerData[1],
            'description' => trim($this->rcon->adminVarGetServerDescription()),
            'game' => $this->server->game,
            'players' => [
                'online' => (int)$rconServerData[2],
                'max' => (int)$rconServerData[3],
                'spectators' => null,
                'commanders' => null,
                'queue' => $this->server->in_queue ?? null,
            ],
            'mode' => [
                'name' => $current_gamemode,
                'uri' => $rconServerData[4],
            ],
            'map' => [
                'name' => $current_map,
                'uri' => $rconServerData[5],
                'next' => $this->getNextMap(),
                'images' => $this->server->map_image_paths,
            ],
            'tickets' => $tickets,
        ];

        return $this;
    }

    /**
     * Gets the next map in the rotation.
     *
     * @return array|null
     * @throws RCONException
     */
    private function getNextMap()
    {
        $index = $this->rcon->adminMaplistGetNextMapIndex();
        $maps = $this->getMapList();

        foreach ($maps as $map) {
            if ($map['index'] == $index) {
                return $map;
            }
        }

        return null;
    }

    /**
     * Gets the map list.
     *
     * @return array
     * @throws RCONException
     */
    private function getMapList()
    {
        $maps = $this->rcon->adminMaplistList();

        $list = [];

        for ($i = 0; $i < $maps[1]; $i++) {
            $map = $maps[($maps[2]) * $i + $maps[2]];
            $mode = $maps[($maps[2]) * $i + $maps[2] + 1];
            $rounds = $maps[($maps[2]) * $i + $maps[2] + 2];

            $_playmode = $this->rcon->getPlaymodeName($mode);
            $_map = $this->rcon->getMapName($map);

            $list[] = [
                'map' => [
                    'name' => $_map,
                    'uri' => $map,
                ],
                'mode' => [
                    'name' => $_playmode,
                    'uri' => $mode,
                ],
                'rounds' => (int)$rounds,
                'index' => $i,
            ];
        }

        return $list;
    }

    /**
     * Simply returns the correct team name by their ID.
     *
     * @param $teamID
     *
     * @return string|null
     */
    protected function getTeamName($teamID)
    {
        return $this->{"team" . $teamID} ?? null;
    }

    /**
     * Check for admins in the player list.
     *
     * @return $this|bool
     */
    private function doGetOnlineAdmins()
    {
        $adminList = Cache::remember('rcon.db.adminlist', Carbon::now()->addDay(), function () {
            return DB::connection('mysql2')->table('adkats_usersoldiers')->select('player_id', 'EAGUID', 'GameID',
                'SoldierName')->join('adkats_users', 'adkats_usersoldiers.user_id', '=',
                'adkats_users.user_id')->join('adkats_roles', 'adkats_users.user_role', '=',
                'adkats_roles.role_id')->join('tbl_playerdata', 'adkats_usersoldiers.player_id', '=',
                'tbl_playerdata.PlayerID')->where('tbl_playerdata.GameID', $this->server->game->GameID)->whereExists(function ($query) {
                $query->select('adkats_rolecommands.role_id')->from('adkats_rolecommands')->join('adkats_commands',
                    'adkats_rolecommands.command_id', '=', 'adkats_commands.command_id')->where('command_playerInteraction',
                    1)->whereRaw('adkats_rolecommands.role_id = adkats_users.user_role')->groupBy('adkats_rolecommands.role_id');
            })->get()->toArray();
        });

        foreach (['players', 'spectators', 'commander'] as $type) {
            foreach ($this->data['teams'] as $teamID => $team) {
                if (array_key_exists($type, $team)) {
                    foreach ($team[$type] as $index => $player) {
                        foreach ($adminList as $index2 => $player2) {
                            $guid = !is_string($player) ? $player['guid'] : $player;

                            if ($guid == $player2->EAGUID) {
                                if ($type == 'commander') {
                                    return false;
                                }

                                $this->data['admins'][$player['name']] = $this->data['teams'][$teamID][$type][$index];
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return mixed
     * @throws Throwable
     */
    public function get()
    {
        return new Collection($this->data);
    }

    /**
     * @param int $length
     * @param string $gamemode
     * @param array $data
     *
     * @return array
     */
    private function serverInfoTicketsBF4(int $length, string $gamemode, array $data)
    {
        $ticketcap = null;
        $uptime = 0;
        $round = 0;

        switch ($gamemode) {
            case 'SquadDeathMatch0':
            case 'TeamDeathMatch0':
                $ticketcap = $length < 28 ? null : intval($data[13]);
                $uptime = $length < 28 ? (int)$data[14] : (int)$data[18];
                $round = $length < 28 ? (int)$data[15] : (int)$data[19];
                break;
            case 'CaptureTheFlag0':
            case 'Obliteration':
            case 'Chainlink0':
            case 'RushLarge0':
            case 'Domination0':
            case 'ConquestLarge0':
            case 'ConquestSmall0':
                $ticketcap = $data[4] == 'CaptureTheFlag0' ? null : ($length < 26 ? null : intval($data[11]));
                $uptime = $length < 26 ? (int)$data[14] : (int)$data[16];
                $round = $length < 26 ? (int)$data[15] : (int)$data[17];
                break;
        }

        return $this->serverInfoTickets($ticketcap, $uptime, $round);
    }

    /**
     * @param int|null $t Ticket Cap
     * @param int|null $u Server Uptime
     * @param int|null $r Round Duration
     *
     * @return array
     */
    private function serverInfoTickets($t, $u, $r)
    {
        return [
            'ticketcap' => $t,
            'uptime' => $u,
            'round' => $r,
        ];
    }

    /**
     * @param int $length
     * @param string $gamemode
     * @param array $data
     *
     * @return array
     */
    private function serverInfoTicketsBF3(int $length, string $gamemode, array $data)
    {
        $ticketcap = null;
        $uptime = 0;
        $round = 0;

        switch ($gamemode) {
            case 'SquadDeathMatch0':
            case 'TeamDeathMatch0':
                $ticketcap = $length < 25 ? null : intval($data[11]);
                $uptime = $length < 25 ? (int)$data[12] : (int)$data[16];
                $round = $length < 25 ? (int)$data[13] : (int)$data[17];
                break;
            case 'CaptureTheFlag0':
            case 'Obliteration':
            case 'Chainlink0':
            case 'RushLarge0':
            case 'Domination0':
            case 'ConquestLarge0':
            case 'ConquestSmall0':
                if ($data[4] == 'CaptureTheFlag0') {
                    $ticketcap = null;
                } else {
                    $ticketcap = $length < 25 ? null : intval($data[11]);
                }
                $uptime = $length < 25 ? (int)$data[12] : (int)$data[16];
                $round = $length < 25 ? (int)$data[13] : (int)$data[17];
                break;
        }

        return $this->serverInfoTickets($ticketcap, $uptime, $round);
    }

    /**
     * @param int $length
     * @param string $gamemode
     * @param array $data
     *
     * @return array
     */
    private function serverInfoTicketsBFH(int $length, string $gamemode, array $data)
    {
        $ticketcap = null;
        $uptime = 0;
        $round = 0;

        switch ($gamemode) {
            case 'TurfWarLarge0':
            case 'TurfWarSmall0':
            case 'Heist0':
            case 'Hotwire0':
            case 'BloodMoney0':
            case 'Hit0':
            case 'Hostage0':
            case 'TeamDeathMatch0':
            case 'CashGrab0':
                $ticketcap = $length < 25 ? null : intval($data[11]);
                $uptime = $length < 25 ? (int)$data[14] : (int)$data[16];
                $round = $length < 25 ? (int)$data[15] : (int)$data[17];
                break;
        }

        return $this->serverInfoTickets($ticketcap, $uptime, $round);
    }
}