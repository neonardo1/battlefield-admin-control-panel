<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Installer',
    'next' => 'Nächster Schritt',
    'finish' => 'Installieren',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Willkommen zum Installer',
        'message' => 'Willkommen zum Laravel Installationsassistent.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Vorraussetzungen',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Berechtigungen',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Umgebungsvariablen',
        'save' => 'Speicher .env',
        'success' => 'Ihre .env Konfiguration wurde gespeichert.',
        'errors' => 'Ihre .env Konfiguration konnte nicht gespeichert werden, Bitte erstellen Sie diese Manuell.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Fertig!',
        'finished' => 'Die Anwendung wurde erfolgreich Installiert.',
        'exit' => 'Hier Klicken zum Beenden',
    ],
];
