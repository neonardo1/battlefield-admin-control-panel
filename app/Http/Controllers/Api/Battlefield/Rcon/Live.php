<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/29/19, 6:49 PM
 */

namespace App\Http\Controllers\Api\Battlefield\Rcon;

use App\Http\Controllers\Controller;
use App\Http\Resources\Rcon as RconResource;
use App\Models\Battlefield\Server\Server;
use App\Repository\Battlefield\Scoreboard\LiveScoreboard;

/**
 * Class Live
 *
 * @package App\Http\Controllers\Api\Battlefield\Rcon
 */
class Live extends Controller
{
    /**
     * @param Server $server
     *
     * @return RconResource|void
     * @throws \Throwable
     */
    public function serverinfo(Server $server)
    {
        $data = new LiveScoreboard($server);
        return new RconResource($data->doGetServerInfo()->get());
    }

    /**
     * @param Server $server
     *
     * @return RconResource
     * @throws \Throwable
     */
    public function players(Server $server)
    {
        $data = new LiveScoreboard($server);
        return new RconResource($data->doGetPlayers()->get());
    }
}
