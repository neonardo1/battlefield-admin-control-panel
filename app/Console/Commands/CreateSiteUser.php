<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/6/19, 2:44 AM
 */

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class CreateSiteUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:create-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->ask("Please enter username");
        $email = $this->ask('Please enter email address');

        if (empty($username) || empty($email)) {
            die("No username provided");
        }

        $display_name = $this->ask("Please enter displayname or press enter to use default", $username);
        $password = Hash::make($this->secret('Please enter password'));

        $defaultPerms = [
            'view.dashboard',
            'view.player.profile',
            'view.player.listing',
            'view.player.history',
        ];

        $user = new User();
        $user->username = $username;
        $user->display_name = $display_name;
        $user->email = $email;
        $user->password = $password;
        $user->api_token = Str::random(60);
        $user->save();

        $role = Role::firstOrCreate(['name' => 'Default']);
        $role->givePermissionTo($defaultPerms);
        $user->assignRole('Default');

        $this->info("User created!");
    }
}
