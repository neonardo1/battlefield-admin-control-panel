<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/15/20, 3:38 AM
 */

namespace Tests\Feature\Routes;

use Tests\TestCase;

/**
 * Class GuestTest
 *
 * @package Tests\Feature\Routes
 */
class GuestTest extends TestCase
{
    public function test_guest_home_page()
    {
        $this->markTestSkipped();

        $this->assertGuest();

        $response = $this->get('/');
        $response->assertSuccessful()->assertViewIs('pages.guest.homepage');
    }

    public function test_guest_home_page_to_login_page()
    {
        $this->markTestSkipped();

        $this->assertGuest();

        $response = $this->get('/login');
        $response->assertSuccessful()->assertViewIs('auth.login');
    }

    public function test_guest_home_page_to_register_page()
    {
        $this->markTestSkipped();

        $this->assertGuest();

        $response = $this->get('/register');
        $response->assertSuccessful()->assertViewIs('auth.register');
    }

    public function test_guest_cant_access_dashboard()
    {
        $this->markTestSkipped();

        $this->assertGuest();

        $response = $this->get('/dashboard');
        $response->assertStatus(302)->assertRedirect('/login');
    }
}
