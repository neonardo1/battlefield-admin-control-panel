<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:05 AM
 */

namespace App\Observers\Battlefield;

use App\Models\Battlefield\Player;

class PlayerObserver
{
    /**
     * Handle the player "created" event.
     *
     * @param \App\Models\Battlefield\Player $player
     *
     * @return void
     */
    public function created(Player $player)
    {
        //
    }

    /**
     * Handle the player "updated" event.
     *
     * @param \App\Models\Battlefield\Player $player
     *
     * @return void
     */
    public function updated(Player $player)
    {
        //
    }

    /**
     * Handle the player "deleted" event.
     *
     * @param \App\Models\Battlefield\Player $player
     *
     * @return void
     */
    public function deleted(Player $player)
    {
        //
    }

    /**
     * Handle the player "restored" event.
     *
     * @param \App\Models\Battlefield\Player $player
     *
     * @return void
     */
    public function restored(Player $player)
    {
        //
    }

    /**
     * Handle the player "force deleted" event.
     *
     * @param \App\Models\Battlefield\Player $player
     *
     * @return void
     */
    public function forceDeleted(Player $player)
    {
        //
    }
}
