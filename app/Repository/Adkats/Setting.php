<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/11/20, 1:05 AM
 */

namespace App\Repository\Adkats;

use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class Setting
 *
 * @package App\Repository\Adkats
 */
class Setting extends Base
{
    /**
     * This returns a unique list of the pre-defined messages stored in AdKats. The data in the list will be cached
     * for 5 minutes to avoid hitting the database every time. List is sorted in ASC order.
     *
     * @return array
     */
    public function predefinedMessages(): array
    {
        return Cache::remember('adkats.setting.predefinedMessages', Carbon::now()->addMinutes(5), static function () {
            /** @var Collection $messages */
            $messages = \App\Models\Adkats\Setting::where('setting_name', '=', 'Pre-Message List')->get();

            return $messages->flatMap(static function ($v, $k) {
                return $v->setting_value;
            })->unique()->sort()->values()->toArray();
        });
    }
}