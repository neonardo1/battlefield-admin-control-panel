<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/19/19, 4:53 AM
 */

return [
    'scoreboard' => [
        'factions' => [
            0 => [
                'full_name' => 'Neutral',
                'short_name' => 'Neutral',
            ],
            1 => [
                'full_name' => 'US Army',
                'short_name' => 'US',
            ],
            2 => [
                'full_name' => 'Russian Army',
                'short_name' => 'RU',
            ],
            3 => [
                'full_name' => 'Chinese Army',
                'short_name' => 'CN',
            ],
            4 => [
                'full_name' => 'Attackers',
                'short_name' => 'Attackers',
            ],
            5 => [
                'full_name' => 'Defenders',
                'short_name' => 'Defenders',
            ],
            6 => [
                'full_name' => 'Cops',
                'short_name' => 'CO',
            ],
            7 => [
                'full_name' => 'Criminals',
                'short_name' => 'CR',
            ],
        ],
    ],
];