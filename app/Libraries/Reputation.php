<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 10:58 AM
 */

namespace App\Libraries;


use App\Models\Adkats\Record;
use App\Models\Battlefield\Player;
use App\Repository\Base;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * Class Reputation
 *
 * @package App\Libraries
 */
class Reputation extends Base
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Collection
     */
    private $weights;

    /**
     * @var float
     */
    private $sourceRep = 0.0;

    /**
     * @var float
     */
    private $targetRep = 0.0;

    /**
     * @var float
     */
    private $totalRep = 0.0;

    /**
     * @var float
     */
    private $totalRepCo = 0.0;

    /**
     * Reputation constructor.
     *
     * @param  Client  $client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->weights();
    }

    /**
     * @return Reputation
     */
    private function weights(): Reputation
    {
        $this->weights = Cache::remember('adkats.reputation.weights', Carbon::now()->addWeek(), function () {
            try {
                try {
                    $request = $this->httpclient->get('https://raw.githubusercontent.com/AdKats/AdKats/master/adkatsreputationstats.json');
                    Log::debug('Fetched reputation weights from primary source.');
                } catch (RequestException $e) {
                    $request = $this->httpclient->get('https://api.myrcon.net/plugins/adkats/reputation');
                    Log::debug('Fetched reputation weights from backup source.');
                }

                return new Collection(json_decode($request->getBody(), true, 512, JSON_THROW_ON_ERROR));
            } catch (\Exception $e) {
                Log::error('Failed to get reputation weights from master or backup sources. Unable to process.');
                throw $e;
            }
        });

        return $this;
    }

    /**
     * @return Collection
     */
    public function results(): Collection
    {
        return new Collection([
            'source' => $this->sourceRep,
            'target' => $this->targetRep,
            'total' => $this->totalRep,
            'calculated' => $this->totalRepCo,
        ]);
    }

    /**
     * @return Collection
     */
    public function getWeights(): Collection
    {
        return $this->weights;
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }

    /**
     * @param  Player  $player
     *
     * @return Reputation
     */
    public function setPlayer(Player $player): Reputation
    {
        $this->resetRep();

        $this->player = $player->load([
            'recordsOn' => static function (HasMany $query) {
                $query->whereIn('command_type', [9, 10]);
            },
        ]);

        $this->sourceWeight()->targetWeight()->special()->calculate();

        return $this;
    }

    /**
     * Resets the rep values for next player
     *
     * @return Reputation
     */
    private function resetRep(): Reputation
    {
        $this->sourceRep = 0.0;
        $this->targetRep = 0.0;
        $this->totalRep = 0.0;
        $this->totalRepCo = 0.0;

        return $this;
    }

    /**
     * @return Reputation
     */
    public function calculate(): Reputation
    {
        $this->totalRep = $this->sourceRep + $this->targetRep;

        $neg = $this->totalRep < 0;
        $negValue = abs($this->totalRep);

        $this->totalRep = $this->helper->divide((1000 * $negValue), ($negValue + 1000));
        $this->totalRepCo = $neg ? -$this->totalRep : $this->totalRep;

        return $this;
    }

    /**
     * @return Reputation
     */
    private function special(): Reputation
    {
        $records = Record::selectRaw('command_type, command_action, COUNT(record_id) AS command_count')
            ->where('target_id', '=', $this->player->PlayerID)
            ->where('source_id', '=', $this->player->PlayerID)
            ->where('command_type', '=', 51)
            ->where('command_action', '=', 51)
            ->groupBy('command_type', 'command_action')
            ->get();

        $records->each(function ($v) {
            $this->weightCal($v, true, true);
        });

        return $this;
    }

    /**
     * @param        $record
     *
     * @param  bool  $source
     * @param  bool  $target
     *
     * @return Reputation
     */
    private function weightCal($record, bool $source = false, bool $target = false): Reputation
    {
        $command = sprintf('%u|%u', $record->command_type, $record->command_action);

        $this->weights->each(function ($v2) use ($target, $source, $record, $command) {
            if ($command === $v2->command_typeaction) {
                if ($source) {
                    $this->sourceRep += $v2->source_weight * $record->command_count;
                }

                if ($target) {
                    $this->targetRep += $v2->target_weight * $record->command_count;
                }
            }
        });

        return $this;
    }

    /**
     * @return Reputation
     */
    private function targetWeight(): Reputation
    {
        $this->player->recordsOn->each(function ($v1) {
            $days = Carbon::now()->diffInDays($v1['record_time']);

            switch ($v1->command_type) {
                // Punish Command
                case 9:
                    if ($days < 50) {
                        $this->targetRep -= 20 * $this->helper->divide(50 - $days, 50);
                    }
                    break;
                // Forgive Command
                case 10:
                    if ($days < 50) {
                        $this->targetRep += 20 * $this->helper->divide(50 - $days, 50);
                    }
                    break;
            }
        });

        $records = Record::selectRaw('command_type, command_action, COUNT(record_id) AS command_count')
            ->whereRaw('target_name != source_name')
            ->where('target_id', '=', $this->player->PlayerID)
            ->groupBy('command_type', 'command_action')
            ->get();

        $records->each(function ($v1) {
            $this->weightCal($v1, false, true);
        });

        return $this;
    }

    /**
     * @return Reputation
     */
    private function sourceWeight(): Reputation
    {
        $records = Record::selectRaw('command_type, command_action, COUNT(record_id) AS command_count')
            ->whereRaw('target_name != source_name')
            ->where('source_id', '=', $this->player->PlayerID)
            ->groupBy('command_type', 'command_action')
            ->get();

        $records->each(function ($v1) {
            $this->weightCal($v1, true);
        });

        return $this;
    }

    /**
     * @return Reputation
     */
    public function update(): Reputation
    {
        $this->player->reputation()->updateOrCreate([
            'game_id' => $this->player->game->GameID,
        ], [
            'target_rep' => $this->getTargetRep(),
            'source_rep' => $this->getSourceRep(),
            'total_rep' => $this->getTotalRep(),
            'total_rep_co' => $this->getTotalRepCo(),
        ]);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetRep()
    {
        return $this->targetRep;
    }

    /**
     * @return float|int
     */
    public function getSourceRep()
    {
        return $this->sourceRep;
    }

    /**
     * @return float|int
     */
    public function getTotalRep()
    {
        return $this->totalRep;
    }

    /**
     * @return float|int
     */
    public function getTotalRepCo()
    {
        return $this->totalRepCo;
    }
}