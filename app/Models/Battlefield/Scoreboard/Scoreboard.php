<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield\Scoreboard;

use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
//use App\Models\Facades\Battlefield as BattlefieldHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Scoreboard.
 * @property mixed Kills
 * @property mixed Headshots
 * @property mixed Deaths
 * @property mixed SquadID
 * @property mixed PlayerJoined
 */
class Scoreboard extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_currentplayers';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ServerID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['PlayerJoined'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['player_joined_iso', 'squad', 'kd_ratio', 'hsk_ratio'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'EA_GUID', 'EAGUID');
    }

    /**
     * @return mixed
     */
    public function getPlayerJoinedIsoAttribute()
    {
        return $this->PlayerJoined->toIso8601String();
    }

    /**
     * @return mixed
     */
    public function getSquadAttribute()
    {
        //return BattlefieldHelper::squad($this->SquadID);
    }

    /**
     * @return mixed
     */
    public function getKdRatioAttribute()
    {
        //return BattlefieldHelper::kd($this->Kills, $this->Deaths);
    }

    /**
     * @return mixed
     */
    public function getHskRatioAttribute()
    {
        //return BattlefieldHelper::hsk($this->Headshots, $this->Kills);
    }
}
