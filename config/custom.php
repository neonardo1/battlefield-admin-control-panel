<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/28/19, 5:26 PM
 */

return [
    'custom' => [

        /**
         * Change the theme options
         *
         * Valid entries
         *
         * 'light'
         * 'dark' (default)
         * 'semi-dark'
         */
        'theme' => 'dark',

        /**
         * Collapse the sidebar on page load
         *
         * Default: false
         */
        'sidebarCollapsed' => false,

        /**
         * Change the navigation bar color. Valid options below.
         *
         * bg-primary, bg-info, bg-warning, bg-success, bg-danger, bg-dark (default: '' for #fff)
         */
        'navbarColor' => '',

        /**
         * Change the menu type.
         *
         * 'fixed' (default)
         * 'static'
         */
        'menuType' => 'fixed',

        /**
         * Change the navigation bar type
         *
         * 'floating' (default)
         * 'static'
         * 'sticky'
         * 'hidden'
         */
        'navbarType' => 'sticky',

        /**
         * Change the footer type.
         *
         * 'static' (default)
         * 'sticky'
         * 'hidden'
         */
        'footerType' => 'static',

        /**
         * Custom body classes. Leave alone unless you know what you're doing.
         */
        'bodyClass' => '',

        /**
         * Enable or disable the page header for the Breadcrumbs
         *
         * Default: true
         */
        'pageHeader' => true,

        /**
         * Change the page layout
         *
         * '' (default)
         * 'content-left-sidebar'
         * 'content-right-sidebar'
         * 'content-detached-left-sidebar'
         * 'content-detached-right-sidebar'
         */
        'contentLayout' => '',

        /**
         * Removes the padding on the page.
         *
         * Leave false
         */
        'blankPage' => false,

        /**
         * Main page layout
         *
         * 'vertical' (default)
         * 'horizontal'
         */
        'mainLayoutType' => 'vertical',
    ],
];
