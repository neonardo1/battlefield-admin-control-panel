<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/19/20, 7:22 AM
 */

namespace App\Libraries;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

/**
 * Class SecurityAuthCode
 *
 * @package App\Libraries
 */
class SecurityAuthCode
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var null|string
     */
    private $securityCode;

    /**
     * @var Collection
     */
    private $generator;

    /**
     * @throws \Throwable
     */
    public function __construct()
    {
        throw_unless(auth()->check(), new \Exception('Not logged in'));
        $this->generator = new Collection();
        $this->user = auth()->user();
        $this->generate();
    }

    /**
     * Generates the security code.
     *
     * @return SecurityAuthCode
     * @throws \Throwable
     */
    private function generate(): SecurityAuthCode
    {
        try {
            for ($i = 0; $i < 2; $i++) {
                $this->generator->push($this->randomGreekWord());
            }

            for ($i = 0; $i < 3; $i++) {
                $this->generator->push($this->randomNumberGenerator());
            }

            $this->generator = $this->generator->shuffle();

            $this->securityCode = $this->user->username.'-'.implode('-', $this->generator->toArray());

            $this->hashAndSaveToUser();

            return $this;
        } catch (\Exception $e) {
            throw new \RuntimeException('Unable to generate security code. Please try again.', 1002);
        }
    }

    /**
     * Picks a random word from the array list.
     *
     * @return string
     */
    private function randomGreekWord(): string
    {
        $words = [
            'alpha',
            'beta',
            'delta',
            'epsilon',
            'gamma',
            'kappa',
            'lambda',
            'omega',
            'omicron',
            'phi',
            'pi',
            'sigma',
            'theta',
        ];

        shuffle($words);

        return $words[array_rand($words)];
    }

    /**
     * Returns a random number.
     *
     * @param  int  $min
     * @param  int  $max
     *
     * @return int
     */
    private function randomNumberGenerator($min = 0, $max = 9): int
    {
        try {
            return random_int($min, $max);
        } catch (\Exception $e) {
            return $this->randomNumberGenerator();
        }
    }

    /**
     * Hashes and stores the security code to the database.
     *
     * @return bool
     * @throws \Throwable
     */
    private function hashAndSaveToUser(): bool
    {
        $this->user->authorization_code = Hash::make($this->getSecurityCode());
        $this->user->authorization_code_rotated = Carbon::now()->addMonths(3);

        return $this->user->saveOrFail();
    }

    /**
     * @return string|null
     */
    public function getSecurityCode(): ?string
    {
        return $this->securityCode;
    }

    /**
     * @param  User  $user
     *
     * @return bool
     * @throws \Throwable
     */
    public static function needsRotation(User $user): bool
    {
        return Carbon::now()->gt($user->authorization_code_rotated);
    }
}