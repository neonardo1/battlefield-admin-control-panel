<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 6/28/20, 8:15 PM
 */

namespace App\Http\Controllers\Api\Battlefield;


use App\Http\Controllers\Controller;
use App\Http\Resources\StandardResponse;
use App\Models\Battlefield\Server\Server as ServerDB;
use App\Repository\Battlefield\Server as ServerRepo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class Server
 *
 * @package App\Http\Controllers\Api\Battlefield
 */
class Server extends Controller
{
    /**
     * @return StandardResponse
     */
    public function listing(): StandardResponse
    {
        $servers = ServerDB::active()->get();

        $population = new Collection([
            'used' => $servers->sum('usedSlots'),
            'total' => $servers->sum('maxSlots'),
        ]);

        return new StandardResponse(new Collection([
            'population' => $population,
            'servers' => $servers,
        ]));
    }

    /**
     * @param  ServerDB  $server
     *
     * @return StandardResponse
     */
    public function punishments(ServerDB $server): StandardResponse
    {
        $punishments = app(ServerRepo::class)->punishmentHierarchy($server);

        return new StandardResponse($punishments);
    }

    /**
     * @param  ServerDB  $server
     *
     * @return StandardResponse
     */
    public function populationGraph(ServerDB $server): StandardResponse
    {
        $chart = Cache::remember('server.'.$server->ServerID.'.populationGraph', Carbon::now()->addMinutes(15),
            static function () use ($server) {
                $server->loadMissing([
                    'maps' => static function ($query) {
                        $query->since(Carbon::now()->subWeeks(2));
                    },
                ]);

                return $server->maps->map(static function ($v) {
                    return [
                        'x' => \Carbon\Carbon::parse($v['TimeRoundEnd'])->timestamp * 1000,
                        'y' => $v['AvgPlayers'],
                    ];
                })->sortBy('x')->values();
            });

        return new StandardResponse($chart);
    }

}
