<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 6/21/20, 10:22 AM
 */

namespace App\Models\Battlefield\Server;

use App\Models\Elegant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stats.
 */
class Maps extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_mapstats';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ServerID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ServerID'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['TimeMapLoad', 'TimeRoundStarted', 'TimeRoundEnd'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [
        'unix',
    ];

    /**
     * The attributes excluded form the models JSON response.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'ServerID');
    }

    /**
     * @param          $query
     * @param  Carbon  $timeframe
     *
     * @return Model
     */
    public function scopeSince($query, Carbon $timeframe)
    {
        return $query->where('TimeMapLoad', '>=', $timeframe)->where(static function ($q) {
            $q->where('TimeRoundStarted', '!=', '0001-01-01 00:00:00');
            $q->where('TimeRoundEnd', '!=', '0001-01-01 00:00:00');
            $q->where('TimeMapLoad', '!=', '0001-01-01 00:00:00');
        });
    }

    /**
     * @return array
     */
    public function getUnixAttribute(): array
    {
        return [
            'TimeMapLoad' => $this->TimeMapLoad->timestamp,
            'TimeRoundStarted' => $this->TimeRoundStarted->timestamp,
            'TimeRoundEnd' => $this->TimeRoundEnd->timestamp,
        ];
    }

    /**
     * @param          $query
     * @param  Carbon  $timeframe
     *
     * @return Model
     */
    public function scopePopular($query, Carbon $timeframe)
    {
        return $query->where('TimeMapLoad', '>=', $timeframe)->where(static function ($q) {
            $q->where('TimeRoundStarted', '!=', '0001-01-01 00:00:00');
            $q->where('TimeRoundEnd', '!=', '0001-01-01 00:00:00');
            $q->where('TimeMapLoad', '!=', '0001-01-01 00:00:00');
        })->where('MapName', '!=', '')
            ->selectRaw("MapName, Gamemode, COUNT(ID) AS 'Total'")
            ->groupBy('MapName');
    }
}
