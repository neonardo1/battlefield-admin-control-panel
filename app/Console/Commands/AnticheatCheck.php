<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/20/19, 8:05 AM
 */

namespace App\Console\Commands;

use App\Libraries\AntiCheat;
use App\Models\Battlefield\Player;
use Illuminate\Console\Command;

class AnticheatCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:acs {player?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the player for any triggered weapons';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (empty($this->argument('player'))) {
            $this->info("No player selected. Doing bulk update. This can take a while...");
        } else {
            $this->singlePlayerCheck($this->argument('player'));
        }
    }

    /**
     * @param $player
     */
    private function singlePlayerCheck($player)
    {
        try {
            $player = Player::findOrFail($player);

            $acs = new AntiCheat($player);

            $triggered = $acs->parse()->get();

            $headers = [
                'Weapon', 'Category', 'Kills', 'Fired', 'Hit', 'Accuracy', 'DPS %', 'HSKP %', 'KPM %', 'DPS Triggered',
                'KPM Triggered', 'HKP Triggered',
            ];

            $data = [];

            $this->info(sprintf("Anticheat Overview for %s in %s", $player->SoldierName, $player->game->Name));

            foreach ($triggered as $t) {
                $data[] = [
                    $t['slug'],
                    $t['category'],
                    number_format($t['kills']),
                    number_format($t['fired']),
                    number_format($t['hit']),
                    $t['accuracy'],
                    $t['dps'],
                    $t['hskp'],
                    $t['kpm'],
                    $t['triggered']['DPS'] ? 'Yes' : 'No',
                    $t['triggered']['KPM'] ? 'Yes' : 'No',
                    $t['triggered']['HKP'] ? 'Yes' : 'No',
                ];
            }

            $this->table($headers, $data);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
