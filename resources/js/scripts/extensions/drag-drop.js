/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:56 PM
 */

/*=========================================================================================
    File Name: drag-drop.js
    Description: drag & drop elements using dragula js
    --------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


$(document).ready(function () {

    // Draggable Cards
    dragula([document.getElementById('card-drag-area')]);

    // Sortable Lists
    dragula([document.getElementById('basic-list-group')]);
    dragula([document.getElementById('multiple-list-group-a'), document.getElementById('multiple-list-group-b')]);

    // Cloning
    dragula([document.getElementById('chips-list-1'), document.getElementById('chips-list-2')], {
        copy: true
    });

    // With Handles

    dragula([document.getElementById("handle-list-1"), document.getElementById("handle-list-2")], {
        moves: function (el, container, handle) {
            return handle.classList.contains('handle');
        }
    });

});
