<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Reset Password - {{ config('bfacp.community_name', 'Battlefield Admin Control Panel') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('vendors/css/vendors.min.css')) }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/bootstrap-extended.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/colors.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/components.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/dark-layout.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/themes/semi-dark-layout.css')) }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/menu/menu-types/vertical-menu.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/colors/palette-gradient.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pages/authentication.css')) }}">
    <!-- END: Page CSS-->
    <style type="text/css">
        html body.bg-full-screen-image {
            background: url({{ asset('images/backgrounds/login-bf4-background.png') }}) no-repeat center center !important;
        }
    </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout dark-layout 1-column footer-static bg-full-screen-image blank-page">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <section class="row flexbox-container">
                <div class="col-xl-4 col-12 d-flex justify-content-center">
                    <div class="card bg-authentication rounded-0 mb-0">
                        <div class="row m-0">
                            <div class="col-lg-12 col-12 p-0">
                                <div class="card rounded-0 mb-0 px-2">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="mb-0">Reset Password</h4>
                                        </div>
                                    </div>
                                    <p class="px-2">Please enter your new password.</p>
                                    <div class="card-content">
                                        <div class="card-body pt-1">
                                            @if (session('status'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                                            <form method="POST" action="{{ route('password.update') }}" id="regForm">
                                                @csrf
                                                <input type="hidden" name="token" value="{{ $token }}">

                                                <fieldset class="form-label-group">
                                                    <!-- <input type="text" class="form-control" id="user-email" placeholder="Email" required> -->
                                                    <input id="email" type="email"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email" placeholder="Email"
                                                           value="{{ $email ?? old('email') }}" required
                                                           autocomplete="email" autofocus>
                                                    <label for="email">Email</label>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                                    @enderror
                                                </fieldset>

                                                <fieldset class="form-label-group">
                                                    <!-- <input type="password" class="form-control" id="user-password" placeholder="Password" required> -->
                                                    <input id="password" type="password"
                                                           class="form-control @error('password') is-invalid @enderror"
                                                           name="password" placeholder="Password" required
                                                           autocomplete="new-password" v-model="password">
                                                    <label for="password">Password</label>
                                                    <div class="help-block" v-show="password.length >= 8">
                                                        <small v-bind:class="passwordClass">
                                                            <i v-bind:class="passwordIcon"></i>
                                                            <span v-text="passwordWarning"
                                                                  v-bind:class="passwordClass"></span>
                                                        </small>
                                                        <ul class="list-style-circle"
                                                            v-if="passwordSuggestions.length > 0">
                                                            <li class="text-small text-info"
                                                                v-for="s in passwordSuggestions">
                                                                @{{ s }}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                                    @enderror
                                                </fieldset>

                                                <fieldset class="form-label-group">
                                                    <!-- <input type="password" class="form-control" id="user-confirm-password" placeholder="Confirm Password" required> -->
                                                    <input id="password-confirm" type="password" class="form-control"
                                                           name="password_confirmation" placeholder="Confirm Password"
                                                           required autocomplete="new-password">
                                                    <label for="password-confirm">Confirm Password</label>
                                                </fieldset>
                                                <div class="row pt-2">
                                                    <div class="col-12 col-md-6 mb-1">
                                                        <a href="{{ route('user.login') }}"
                                                           class="btn btn-outline-primary btn-block px-0">
                                                            Go Back to Login
                                                        </a>
                                                    </div>
                                                    <div class="col-12 col-md-6 mb-1">
                                                        <button type="submit" class="btn btn-primary btn-block px-0">
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="login-footer">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- END: Content-->

<!-- BEGIN: Vendor JS-->
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/register.js')) }}"></script>
<!-- END: Theme JS-->

</body>
<!-- END: Body-->

</html>