<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 6/28/20, 7:58 PM
 */

namespace App\Http\Controllers\Battlefield;


use App\Http\Controllers\Controller;
use App\Models\Battlefield\Server\Server as ServerDB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


/**
 * Class ServerController
 *
 * @package App\Http\Controllers\Battlefield
 */
class ServerController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $servers = Cache::remember('servers.list.page', Carbon::now()->addMinutes(5), static function () {
            return ServerDB::with(['stats'])
                ->active()
                ->orderBy('GameID')
                ->orderBy('ServerName')
                ->get();
        });

        return view('pages.battlefield.server.index', compact('servers'));
    }
}
