@extends('layouts/contentLayoutMaster')

@section('title', 'Server List')

@section('vendor-style')
    <!-- vednor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
@endsection

@section('vendor-script')
    <!-- vednor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
@endsection

@section('mystyle')
    <!-- Page css files -->
@endsection

@section('myscript')
    <!-- Page js files -->
    <script type="application/javascript">
        $(document).ready(function () {
            function populationGraph(serverid, el, servername) {
                // Chart options
                let options = {
                    chart: {
                        type: 'line',
                        height: '300px',
                        stacked: false
                    },
                    xaxis: {
                        type: 'datetime'
                    },
                    series: [],
                    stroke: {
                        curve: 'smooth',
                    },
                    noData: {
                        text: 'Loading...'
                    },
                    title: {
                        text: 'Population History - ' + servername,
                        align: 'left'
                    },
                    yaxis: {
                        'title': {
                            text: 'Players'
                        },
                        labels: {
                            formatter: function (val) {
                                return (val).toFixed(0);
                            },
                        },
                    },
                    tooltip: {
                        shared: false,
                        y: {
                            formatter: function (val) {
                                return (val).toFixed(0)
                            }
                        }
                    }
                };

                // Initialization
                let chart = new ApexCharts(
                    document.querySelector("#" + el),
                    options
                );

                chart.render();

                axios.get('/api/server/' + serverid + '/population/chart').then(function (response) {
                    chart.updateSeries([{
                        name: 'Players',
                        data: response.data.data
                    }]);
                });

                return chart;
            }

            @foreach($servers as $server)
            populationGraph({{ $server->ServerID }}, 'population-chart-server-{{ $server->ServerID }}', '{{ $server->ServerName }}')
            @endforeach
        });
    </script>
@endsection

@section('content')
    <section>
        @foreach($servers as $server)
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header mb-1">
                            <h4 class="card-title">
                            <span class="badge badge-pill {{ $server->game->class_css }}">
                                {{ $server->game->Name }}
                            </span>
                                {{ $server->ServerName }}
                            </h4>
                        </div>
                        <div class="card-content">
                            <img class="card-img img-fluid" src="{{ asset($server->map_image_paths['wide']) }}"
                                 alt="{{ $server->current_map }}">
                            <div class="card-img-overlay overflow-hidden">
                                <p class="card-text text-white overlay-dark mt-4">
                                    {{ $server->current_map }} ({{ $server->current_gamemode }})
                                    <span class="pull-right">
                                        Players Online&colon;&nbsp;
                                        {{ $server->usedSlots }} / {{ $server->maxSlots }}
                                        @unless($server->in_queue === null || $server->in_queue === 0)
                                            <span class="badge badge-warning">
                                                {{ $server->in_queue ?? 0 }}
                                            </span>
                                        @endunless
                                    </span>
                                </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-dark">
                                    <thead>
                                    <tr>
                                        <th>Players Visited</th>
                                        <th>Avg Player Score</th>
                                        <th>Avg Player Kills</th>
                                        <th>Avg Player Deaths</th>
                                        <th>Avg Playtime</th>
                                        <th>Rounds Played</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @unless(is_null($server->stats))
                                        <tr>
                                            <td>{{ number_format($server->stats->CountPlayers) }}</td>
                                            <td>{{ number_format($server->stats->AvgScore) }}</td>
                                            <td>{{ number_format($server->stats->AvgKills) }}</td>
                                            <td>{{ number_format($server->stats->AvgDeaths) }}</td>
                                            <td>{{ app(\App\Helpers\Main::class)->secToStr($server->stats->AvgPlaytime, true) }}</td>
                                            <td>{{ number_format($server->stats->SumRounds) }}</td>
                                        </tr>
                                    @endunless
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div id="population-chart-server-{{ $server->ServerID }}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
@endsection
