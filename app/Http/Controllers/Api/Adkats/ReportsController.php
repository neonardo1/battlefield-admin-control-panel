<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Http\Controllers\Api\Adkats;


use App\Http\Controllers\Controller;
use App\Models\Adkats\Record;

/**
 * Class ReportsController
 * @package App\Http\Controllers\Api\Adkats
 */
class ReportsController extends Controller
{
    /**
     * @param bool $paginate
     * @param int $limit
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function latestReports($paginate = false, $limit = 30)
    {
        $data = Record::with('server', 'type', 'action')
            ->whereIn('command_type', [18,20])
            ->orderBy('record_time', 'desc');

        if(request()->has('last_id')) {
            $lastId = request()->get('last_id', null);

            if(is_numeric($lastId)) {
                $data->where('record_id', '>', abs($lastId));
            }
        }

        if(!$paginate) {
            $reports = $data->paginate($limit);
        } else {
            $reports = $data->take($limit)->get();
        }

        return $reports;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function report($id)
    {
        $report = Record::with('server', 'type', 'action')
            ->whereIn('command_type', [18, 20])
            ->findOrFail($id);

        return $report;
    }
}