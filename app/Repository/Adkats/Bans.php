<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/6/19, 2:17 AM
 */

namespace App\Repository\Adkats;

use App\Models\Adkats\Ban;
use App\Models\Adkats\Record;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Bans
 *
 * @package App\Repository\Adkats
 */
class Bans extends Base
{
    /**
     * @param int $limit
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latest($limit = 30)
    {
        $bans = Ban::with('player', 'record')
            //->active()
            ->orderBy('ban_startTime', 'desc')
            ->paginate(30);

        return $bans;
    }

    /**
     * Gets the total bans done yesterday
     *
     * @return int
     */
    public function yesterday(): int
    {
        $bans = Cache::remember('adkats.bans.stats.yesterday', Carbon::now()->addDay(), function () {
            return Ban::yesterday()->count();
        });

        return $bans;
    }

    /**
     * Gets the average bans done in $months
     *
     * @param int $months
     *
     * @return int
     */
    public function average(int $months = 1): int
    {
        $bans = Cache::remember('adkats.bans.stats.average', Carbon::now()->addDay(), function () use ($months) {
            $history = DB::connection('mysql2')->select(DB::raw("
                SELECT 
                    FORMAT(AVG(a.total), 0) AS 'total'
                FROM
                    (SELECT 
                        COUNT(`ban_id`) AS 'total'
                    FROM
                        `adkats_bans`
                    WHERE
                        `ban_startTime` >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL ? MONTH)
                    GROUP BY YEAR(`ban_startTime`) , MONTH(`ban_startTime`) , DAY(`ban_startTime`)) AS a
            "), [$months]);

            return (int)head($history)->total;
        });

        return $bans;
    }


    /**
     * Gets the bans that were issued in a month to be used in the chart
     *
     * @return Collection
     */
    public function bansIssuedGraph(): Collection
    {
        $bans = Cache::remember('adkats.bans.stats.history', Carbon::now()->addDay(), function () {
            /** @var \Illuminate\Support\Collection $records */
            $records = Record::selectRaw('DATE(record_time) as date, COUNT(record_id) as total')
                ->whereIn('command_action', [7, 8])
                ->whereRaw('record_time != DATE(\'0001-01-01\') AND record_time >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 6 MONTH)')
                ->groupBy(DB::raw('DATE(record_time)'))
                ->get()
                ->map(function ($v) {
                    return [
                        'x' => $v['date'] . ' UTC',
                        'y' => $v['total'],
                    ];
                });

            return $records;
        });

        return $bans;
    }
}