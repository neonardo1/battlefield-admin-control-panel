<div class="modal fade" id="playerRepGuideModal" tabindex="-1" role="dialog" aria-labelledby="playerRepGuideModal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="playerRepGuideModalTitle">How The Player Reputation System Works</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Reputation is a numeric for how helpful a player is to the server. The more they help admins by
                    reporting rule breakers, moreso from spectator, assisting the weak team, or populating the server,
                    the more their reputation increases. Committing infractions, breaking server rules, getting banned,
                    etc, reduces their server reputation.</p>

                <p>
                    Reputation starts at zero and moves toward <span class="text-info">-1000</span> or <span
                            class="text-info">1000</span> so it's easy to get/lose rep early on but
                    harder near the top/bottom. Players will never reach <span class="text-info">-1000</span> or
                    <span class="text-info">1000</span> reputation, but can get close
                    with a lot of effort. Each command a player issues and every command issued against them has a
                    reputation amount; Some good, some bad. Every time a player's reputation changes you are notified of
                    the change in chat.
                </p>

                <p>The following are ways reputation can be gained:</p>

                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 text-danger">Issuing good reports on players.</h5>
                        </div>

                        <p>Just reporting someone gives rep but when an admin accepts the report or acts on it it's
                            triple the rep bonus.</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 text-danger">Reporting from spectator.</h5>
                        </div>

                        <p>Reporting from spectator is worth much more than
                            reporting in-game. Players are sacrificing their game time to help a server and should be
                            rewarded.</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 text-danger">Using !assist.</h5>
                        </div>

                        <p>Sometimes teams really need help, and sometimes a player's
                            friends are stuck on the weak team. Helping them and the server out by using this command to
                            switch increases rep greatly.</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 text-danger">Populating servers.</h5>
                        </div>

                        <p>Worth twice that of an assist, populating a server helps
                            more than almost anything else. Players are notified and thanked for populating servers
                            along
                            with the rep bonus.</p>
                    </li>
                </ul>

                <p>If a player has infractions on their record that causes a reputation reduction, but the reduction
                    infraction points cause reduces over time. So if they have infractions on their record, simply not
                    committing them for a while reduces the rep loss caused. It does not reduce completely however, they
                    will need to report some rule breakers to get it positive again.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-relief-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>