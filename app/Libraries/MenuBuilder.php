<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 10:59 AM
 */

namespace App\Libraries;


use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class MenuBuilder
 *
 * @package App\Libraries
 */
class MenuBuilder
{
    /**
     * @var Collection
     */
    private $menu;

    /**
     * MenuBuilder constructor.
     */
    public function __construct()
    {
        $this->menu = new Collection([]);
    }

    /**
     * @param  string  $name
     * @param  string  $icon
     *
     * @return $this
     */
    public function addNavHeader(string $name, string $icon = ''): MenuBuilder
    {
        $this->menu->add([
            'url' => '',
            'navheader' => $name,
            'icon' => $icon,
        ]);

        return $this;
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        return new Collection([
            'menu' => $this->menu,
        ]);
    }

    /**
     * @param  string  $route
     * @param  string  $name
     * @param  string  $icon
     * @param  array   $meta
     * @param  null    $func
     *
     * @return Collection
     */
    public function item(string $route, string $name, $icon = '', $meta = [], $func = null): Collection
    {
        $item = new Collection([
            'url' => $route,
            'name' => $name,
            'slug' => Str::slug($name),
        ]);

        if (!empty($icon)) {
            $item->put('icon', $icon);
        }

        if (!empty($meta)) {
            $item->merge($meta);
        }

        if ($func !== null) {
            $item->put('submenu', call_user_func($func));
        }

        return $item;
    }

    /**
     * @param  Collection  $item
     *
     * @return MenuBuilder
     */
    public function add(Collection $item): MenuBuilder
    {
        $this->menu->add($item);

        return $this;
    }
}