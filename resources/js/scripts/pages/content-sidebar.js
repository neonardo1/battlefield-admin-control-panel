/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:56 PM
 */

/*=========================================================================================
    File Name: content-sidebar.js
    Description: Invoices list datables configurations
    ----------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function () {
    /***********************************
     *        js of small Slider        *
     ************************************/

    var sm_options = {
        start: [30, 70],
        behaviour: 'drag',
        connect: true,
        range: {
            'min': 20,
            'max': 80
        }
    };
    var smallSlider = document.getElementById('small-slider');
    noUiSlider.create(smallSlider, sm_options);


    if ($(".sidebar-sticky").length) {
        var headerNavbarHeight,
            footerNavbarHeight;

        // Header & Footer offset only for right & left sticky sidebar
        if ($("body").hasClass('content-right-sidebar') || $("body").hasClass('content-left-sidebar')) {
            headerNavbarHeight = $('.header-navbar').height();
            footerNavbarHeight = $('footer.footer').height();
        }
        // Header & Footer offset with padding for detached right & left dsticky sidebar
        else {
            headerNavbarHeight = $('.header-navbar').height() + 24;
            footerNavbarHeight = $('footer.footer').height() + 10;
        }

        $(".sidebar-sticky").sticky({
            topSpacing: headerNavbarHeight,
            bottomSpacing: footerNavbarHeight
        });

    }
});