<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2020. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 1/21/20, 5:07 AM
 */

namespace App\Repository\Battlefield\Player;


use App\Models\Adkats\Record;
use App\Models\Battlefield\Player as PlayerDB;
use App\Repository\Base;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Player
 *
 * @package App\Repository\Battlefield\Player
 */
class Player extends Base
{
    /**
     * Gets the new players for the last $days
     *
     * @param  int   $days
     * @param  bool  $clearCache
     *
     * @return Collection
     */
    public function newPlayersHistory(int $days = 30, bool $clearCache = false): Collection
    {
        $cacheKey = 'dashboard.player.history';

        if ($clearCache) {
            $this->helper->clearCache($cacheKey);
        }

        // New Player History for the last 30 days
        return Cache::remember($cacheKey, Carbon::now()->addDay(),
            static function () use (&$days) {
                return PlayerDB::selectRaw('COUNT(PlayerID) AS Total')
                    ->whereRaw('created_at >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL '.$days.' DAY)')
                    ->groupBy(DB::raw('DATE(created_at)'))
                    ->orderByRaw('DATE(created_at)')
                    ->get()
                    ->map(static function ($v) {
                        return $v['Total'];
                    });
            });
    }

    /**
     * Gets the total unique players across all games.
     *
     * @param  bool  $clearCache
     *
     * @return int
     */
    public function totalUniquePlayers(bool $clearCache = false): int
    {
        $cacheKey = 'dashboard.player.total';

        if ($clearCache) {
            $this->helper->clearCache($cacheKey);
        }

        return Cache::remember($cacheKey, Carbon::now()->addDay(), static function () {
            return PlayerDB::select('eaguid')->distinct()->count();
        });
    }

    /**
     * @param  PlayerDB|null  $player
     *
     * @return Collection|null
     */
    public function cmdOverviewChart(PlayerDB $player)
    {
        $sql = "SELECT 
    command_name AS 'label', SUM(command_count) AS 'total'
FROM
    ((SELECT 
        tb2.command_name, COUNT(tb1.record_id) AS command_count
    FROM
        adkats_records_main tb1
    RIGHT JOIN adkats_commands tb2 ON tb1.command_type = tb2.command_id
    WHERE
        tb1.source_id = ?
    GROUP BY tb1.command_type) UNION ALL (SELECT 
        command_name, 0 AS command_count
    FROM
        adkats_commands
    WHERE
        adkats_commands.command_playerInteraction = 1
            AND command_active = 'Active')) x
GROUP BY command_name
HAVING `total` > 0
ORDER BY `label`";

        $key = sprintf('player.overview.%s', $player->PlayerID);

        /** @var Collection $results */
        $results = Cache::remember($key, Carbon::now()->addDay(), function () use ($sql, $player) {
            $results = new Collection(DB::connection('mysql2')->select(DB::raw($sql), [$player->PlayerID]));

            $labels = $results->mapToDictionary(function ($v, $k) {
                return [
                    'labels' => $v->label,
                ];
            });

            $series = $results->mapToDictionary(function ($v, $k) {
                return [
                    'series' => (int) $v->total,
                ];
            });

            return $labels->merge($series);
        });

        return $results;
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return Collection
     */
    public function linkedAccounts(PlayerDB $player): Collection
    {
        return PlayerDB::with('aliases', 'iphistory')
            ->where('PlayerID', '!=', $player->PlayerID)->where(function ($query) use ($player) {
                if (!empty($player->EAGUID)) {
                    $query->orWhere('EAGUID', $player->EAGUID);
                }

                if (!empty($player->PBGUID)) {
                    $query->orWhere('PBGUID', $player->PBGUID);
                }

                if (!empty($player->SoldierName)) {
                    $query->orWhere('SoldierName', $player->SoldierName);
                }

                if (!empty($player->IP_Address)) {
                    $query->orWhere('IP_Address', $player->IP_Address);
                }

                if ($player->aliases->count() > 0) {
                    $query->orWhereIn('SoldierName', $player->aliases->pluck('record_message'));
                }

                if ($player->iphistory->count() > 0) {
                    $query->orWhereIn('IP_Address', $player->iphistory->pluck('record_message'));
                }
            })->get();
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return Collection
     */
    public function aliases(PlayerDB $player): Collection
    {
        $aliasesUsage = $player->recordsOn()->where('command_type', 48)
            ->selectRaw('record_message AS `player_name`, COUNT(record_id) AS `seen`')
            ->groupBy('player_name')
            ->get();

        $aliasesUsage = new Collection($aliasesUsage);

        $labels = $aliasesUsage->mapToDictionary(function ($v, $k) {
            return [
                'labels' => $v->player_name,
            ];
        });

        $series = $aliasesUsage->mapToDictionary(function ($v, $k) {
            return [
                'series' => (int) $v->seen,
            ];
        });

        $aliasesGraph = $labels->merge($series);

        return new Collection([
            'list' => $aliasesUsage,
            'graph' => $aliasesGraph,
        ]);
    }

    /**
     * @param  PlayerDB  $player
     *
     * @return mixed
     */
    public function history(PlayerDB $player)
    {
        // Increase PHP memory limit for this function only.
        // This can handle at least 20k records being stored into memory.
        // Do not go above 512MB
        ini_set('memory_limit', '512M');

        $key = sprintf('player.%s.records', $player->PlayerID);

        // Exclude any records that deal with player ip or name change. These are in another function.
        return Cache::remember($key, Carbon::now()->addMinutes(15), static function () use ($player) {
            return Record::with('action', 'type', 'server')
                ->whereNotIn('command_type', [48, 49])
                ->where(static function ($query) use ($player) {
                    $query->where('target_id', $player->PlayerID)
                        ->orWhere('source_id', $player->PlayerID);
                })->get();
        });
    }
}