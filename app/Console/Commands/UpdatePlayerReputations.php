<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/4/19, 9:05 PM
 */

namespace App\Console\Commands;

use App\Libraries\Reputation;
use App\Models\Battlefield\Player;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class UpdatePlayerReputations
 *
 * @package App\Console\Commands
 */
class UpdatePlayerReputations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:reputation-updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all player reputations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        /** @var Reputation $reputation */
        $reputation = app(Reputation::class);

        $totalPlayers = Player::count();

        $bar = $this->output->createProgressBar($totalPlayers);

        Player::chunk(10000, function ($players) use ($reputation, $bar) {
            try {
                DB::transaction(function () use ($bar, $reputation, $players) {
                    foreach ($players as $player) {
                        $reputation->setPlayer($player)->update();
                        $bar->advance();
                    }
                });
            } catch (\Exception $e) {
                Log::error($e->getMessage(), $e->getTrace());
            };
        });

        $bar->finish();
    }
}
