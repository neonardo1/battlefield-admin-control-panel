/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:57 PM
 */

import Picker from './picker';


class IconPicker extends Picker {
    constructor(select, icons) {
        super(select);
        this.container.classList.add('ql-icon-picker');
        [].forEach.call(this.container.querySelectorAll('.ql-picker-item'), (item) => {
            item.innerHTML = icons[item.getAttribute('data-value') || ''];
        });
        this.defaultItem = this.container.querySelector('.ql-selected');
        this.selectItem(this.defaultItem);
    }

    selectItem(item, trigger) {
        super.selectItem(item, trigger);
        item = item || this.defaultItem;
        this.label.innerHTML = item.innerHTML;
    }
}


export default IconPicker;
