/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:56 PM
 */

/*=========================================================================================
    File Name: components-modal.js
    Description: Modals are streamlined, but flexible, dialog prompts with the minimum
				required functionality and smart defaults.
    ----------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: Pixinvent
    Author URL: hhttp://www.themeforest.net/user/pixinvent
==========================================================================================*/
(function (window, document, $) {
    'use strict';

    // onShow event
    $('#onshowbtn').on('click', function () {
        $('#onshow').on('show.bs.modal', function () {
            alert('onShow event fired.');
        });
    });

    // onShown event
    $('#onshownbtn').on('click', function () {
        $('#onshown').on('shown.bs.modal', function () {
            alert('onShown event fired.');
        });
    });

    // onHide event
    $('#onhidebtn').on('click', function () {
        $('#onhide').on('hide.bs.modal', function () {
            alert('onHide event fired.');
        });
    });

    // onHidden event
    $('#onhiddenbtn').on('click', function () {
        $('#onhidden').on('hidden.bs.modal', function () {
            alert('onHidden event fired.');
        });
    });
})(window, document, jQuery);