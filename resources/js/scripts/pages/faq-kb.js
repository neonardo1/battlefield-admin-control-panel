/*
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/31/19, 8:56 PM
 */

/*=========================================================================================
    File Name: app-todo.js
    Description: app-todo
    ----------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
    "use strict";

    // Filter
    $("#searchbar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        if (value != "") {
            $(".search-content-info .search-content").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            var search_row = $(".search-content-info .search-content:visible").length;

            //Check if search-content has row or not
            if (search_row == 0) {
                $('.search-content-info .no-result').removeClass('no-items');
            } else {
                if (!$('.search-content-info .no-result').hasClass('no-items')) {
                    $('.search-content-info .no-result').addClass('no-items');
                }
            }
        } else {
            // If filter box is empty
            $(".search-content-info .search-content").show();
            if ($('.search-content-info .no-results').hasClass('no-items')) {
                $('.search-content-info .no-results').removeClass('no-items');
            }
        }
    });

});
