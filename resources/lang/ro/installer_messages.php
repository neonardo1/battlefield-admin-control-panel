<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/22/19, 4:18 AM
 */

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Procesul de instalare Laravel',
    'next' => 'Pasul următor',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title' => 'Bun venit în procesul de instalare...',
        'message' => 'Bun venit în configurarea asistată.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Cerințe',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permisiuni',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Settări ale mediului',
        'save' => 'Salvează fișier .env',
        'success' => 'Setările tale au fost salvate în fișierul .env.',
        'errors' => 'Nu am putut salva fișierul .env, Te rugăm să-l creezi manual.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Am terminat',
        'finished' => 'Aplicația a fost instalată cu succes.',
        'exit' => 'Click aici pentru a ieși',
    ],
];
