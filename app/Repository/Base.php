<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 12/1/19, 2:35 AM
 */

namespace App\Repository;


use App\Helpers\Main;
use GuzzleHttp\Client;

/**
 * Class Base
 *
 * @package App\Repository
 */
class Base
{
    /**
     * @var Client
     */
    protected $httpclient;

    /**
     * @var Main
     */
    protected $helper;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->httpclient = $client;
        $this->helper = app(Main::class);
    }
}