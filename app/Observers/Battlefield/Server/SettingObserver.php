<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:05 AM
 */

namespace App\Observers\Battlefield\Server;

use App\Models\Battlefield\Server\Setting;
use Illuminate\Support\Facades\Cache;

class SettingObserver
{
    /**
     * Handle the setting "created" event.
     *
     * @param \App\Models\Battlefield\Server\Setting $setting
     *
     * @return void
     */
    public function created(Setting $setting)
    {
        Cache::forget('guest.home.serverlist');
    }

    /**
     * Handle the setting "updated" event.
     *
     * @param \App\Models\Battlefield\Server\Setting $setting
     *
     * @return void
     */
    public function updated(Setting $setting)
    {
        Cache::forget('guest.home.serverlist');
    }

    /**
     * Handle the setting "deleted" event.
     *
     * @param \App\Models\Battlefield\Server\Setting $setting
     *
     * @return void
     */
    public function deleted(Setting $setting)
    {
        //
    }

    /**
     * Handle the setting "restored" event.
     *
     * @param \App\Models\Battlefield\Server\Setting $setting
     *
     * @return void
     */
    public function restored(Setting $setting)
    {
        //
    }

    /**
     * Handle the setting "force deleted" event.
     *
     * @param \App\Models\Battlefield\Server\Setting $setting
     *
     * @return void
     */
    public function forceDeleted(Setting $setting)
    {
        //
    }
}
