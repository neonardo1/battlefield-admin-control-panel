<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/27/19, 3:02 AM
 */

namespace App\Providers;

use App\Models\Adkats\Ban;
use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Models\Battlefield\Server\Setting;
use App\Observers\Adkats\BanObserver;
use App\Observers\Battlefield\PlayerObserver;
use App\Observers\Battlefield\Server\ServerObserver;
use App\Observers\Battlefield\Server\SettingObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Server::observe(ServerObserver::class);
        Setting::observe(SettingObserver::class);
        Player::observe(PlayerObserver::class);
        Ban::observe(BanObserver::class);
    }
}
